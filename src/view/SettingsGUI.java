package view;

import java.util.function.UnaryOperator;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;
import main.PeaceMaker;
import model.Pattern;
import model.Settings;

public class SettingsGUI extends Application {
	
	private int row;
	
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Game Of Life Setup");
        this.row = 1;
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(10);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));
        
        Label sizeLbl = new Label("World Size");
        TextField sizeSelector = new TextField();
        setNumericFilter(sizeSelector, Settings.SIZE);
        grid.add(sizeLbl, 1, this.row);
        grid.add(sizeSelector, 2, this.row);
        this.row++;        
        
        Label patternLbl = new Label("Pattern");
        ChoiceBox<Pattern> patternSelector = new ChoiceBox<>(FXCollections.observableArrayList(Pattern.values()));
        patternSelector.setValue(Settings.PATTERN);
        grid.add(patternLbl, 1, this.row);
        grid.add(patternSelector, 2, this.row);
        this.row++;
        
        Label threadsLbl = new Label("Num Threads");
        TextField threadSelector = new TextField();
        setNumericFilter(threadSelector, Settings.NUM_THREADS);
        grid.add(threadsLbl, 1, this.row);
        grid.add(threadSelector, 2, this.row);
        this.row++;
        
        Label waitLbl = new Label("Wait millis");
        TextField waitSelector = new TextField();
        setNumericFilter(waitSelector, Settings.WAIT_MS);
        CheckBox waitChk = new CheckBox("Disabled");
        waitChk.selectedProperty().addListener(e -> {
        	waitSelector.setDisable(waitChk.isSelected());
        });
        grid.add(waitLbl, 1, this.row);
        grid.add(waitSelector, 2, this.row);
        grid.add(waitChk, 3, this.row);
        this.row++;
        
        Label scaleLbl = new Label("Scale");
        TextField scaleSelector = new TextField();
        setNumericFilter(scaleSelector, Settings.SCALE);
        grid.add(scaleLbl, 1, this.row);
        grid.add(scaleSelector, 2, this.row);
        this.row++;
        
        
        Button startBtn = new Button("Load World");
        startBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	Settings.SIZE = new Integer(sizeSelector.getText());
            	Settings.PATTERN = patternSelector.getValue();
            	Settings.NUM_THREADS = new Integer(threadSelector.getText());
            	if (waitChk.isSelected()) {
            		Settings.WAIT_MS = 0;
            	} else {
            		Settings.WAIT_MS = new Integer(waitSelector.getText());	
            	}
            	Settings.createMatrix();
            	Settings.SCALE = new Integer(scaleSelector.getText());
            	GameGUI gui = new GameGUI();
            	primaryStage.setScene(gui.init(primaryStage));
            	PeaceMaker controller = new PeaceMaker(gui);
            	controller.start();
            	gui.setController(controller);	
            }
        });
        
        grid.add(startBtn, 2, this.row);
        this.row++;
        
        Scene scene = new Scene(grid, 400, 200+(20*this.row));
        primaryStage.setScene(scene);
        primaryStage.show();

    }
    
    public static void setNumericFilter(TextField txt, int defaultValue) {
    	UnaryOperator<Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("([1-9][0-9]*)?") && newText.length() < 7) { 
                return change;
            }
            return null;
        };
        txt.setTextFormatter(new TextFormatter<Integer>(new IntegerStringConverter(), defaultValue, integerFilter));
    }
    
    @Override
    public void stop() {
    	System.exit(0);
    }
}
