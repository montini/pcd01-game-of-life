package view;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.util.concurrent.CountDownLatch;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.PixelWriter;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import main.PeaceMaker;
import model.Settings;

public class GameGUI implements EventHandler<ActionEvent>  {

	
	private PeaceMaker controller;

	private Button startBtn;
	private Button stopBtn;
	private CheckBox displayCb;
	private Button bulkStartBtn;
	private Button bulkStopBtn;
	private TextField bulkTxt;

	private ScrollPane mainScrollPane;
	private PixelWriter writer;
	private Canvas canvas;
	
	private Label elapsedLbl;
	private Label aliveLbl;
	private Label epochLbl;
	private Label avgLbl;


	public void refreshMatrix() {
		if (this.displayCb.selectedProperty().getValue()) {

			final CountDownLatch doneLatch = new CountDownLatch(1);
			Platform.runLater(() -> {
				try {
					byte[][] matrix = this.controller.getMatrix();
					int size = matrix.length;
					
					int startX = 0;
					int endX = size;
					
					int startY = 0;
					int endY = size;
					
					if (mainScrollPane.getWidth() < size) {
						startX = (int) (mainScrollPane.getHvalue() * (size - mainScrollPane.getWidth()));
						endX = (int) (startX + mainScrollPane.getWidth());
					}

					if (mainScrollPane.getHeight() < size) {
						startY = (int) (mainScrollPane.getVvalue() * (size - mainScrollPane.getHeight()));
						endY = (int) (startY + mainScrollPane.getHeight());
					}

					for (int i = startX; i < endX; i++) {
						for (int j = startY; j < endY; j++) {
							this.writer.setColor(i, j, (matrix[i][j] == 1? Color.GREEN : Color.BLACK));
						}
					}
					
				} finally {
					doneLatch.countDown();
				}
			});

			try {
				doneLatch.await();
			} catch (InterruptedException e) {}
		}
	}

	public void updateCount(int count) {
		Platform.runLater(() -> epochLbl.setText(""+count));
	}

	public void updateAlive(int alive) {
		Platform.runLater(() -> aliveLbl.setText(""+alive));
	}

	public void updateElapsed(long elasped) {
		Platform.runLater(() -> elapsedLbl.setText(elasped+"ms"));
	}

	public void updateAvg(double avg) {
		Platform.runLater(() -> avgLbl.setText(avg+"ms"));
	}

	public Scene init(Stage primaryStage) {
		primaryStage.setTitle("Game of Life");

		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		if (Settings.SIZE > height) {
			Screen screen = Screen.getPrimary();
			Rectangle2D bounds = screen.getVisualBounds();

			primaryStage.setX(bounds.getMinX());
			primaryStage.setY(bounds.getMinY());
			primaryStage.setWidth(bounds.getWidth());
			primaryStage.setHeight(bounds.getHeight());
		} else if(Settings.SIZE < 500) {
			width = 520;
			height = Settings.SIZE+72;
		} else {
			width = Settings.SIZE+35;
			height = Settings.SIZE+72;
		}

		Scene scene = new Scene(new VBox(), width, height);


		HBox buttonBox = new HBox(10);
		buttonBox.setAlignment(Pos.CENTER_LEFT);
		buttonBox.setPadding(new Insets(10, 0, 10, 20));

		startBtn = new Button("Start");
		startBtn.setOnAction(this);
		buttonBox.getChildren().add(startBtn);

		stopBtn = new Button("Stop");
		stopBtn.setOnAction(this);
		stopBtn.setDisable(true);
		buttonBox.getChildren().add(stopBtn);

		buttonBox.getChildren().add(new Separator(Orientation.VERTICAL));

		displayCb = new CheckBox("Show Grid");
		displayCb.selectedProperty().set(true);
		displayCb.setOnAction(this);
		buttonBox.getChildren().add(displayCb);

		buttonBox.getChildren().add(new Separator(Orientation.VERTICAL));

		bulkStartBtn = new Button("Launch");
		bulkStartBtn.setOnAction(this);
		bulkStopBtn = new Button("Stop");
		bulkStopBtn.setOnAction(this);
		bulkStopBtn.setDisable(true);
		bulkTxt = new TextField(Settings.BULK+"");
		bulkTxt.setPrefWidth(60);
		SettingsGUI.setNumericFilter(bulkTxt,Settings.BULK);
		buttonBox.getChildren().addAll(new Label("Compute bulk"), bulkTxt,bulkStartBtn,bulkStopBtn);


		ScrollPane scrollButtonPane = new ScrollPane();
		scrollButtonPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
		scrollButtonPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		scrollButtonPane.setContent(buttonBox);


		this.canvas = new Canvas(Settings.SIZE,Settings.SIZE);
		this.writer = canvas.getGraphicsContext2D().getPixelWriter();

		canvas.setScaleX(Settings.SCALE);
		canvas.setScaleY(Settings.SCALE);

		this.mainScrollPane = new ScrollPane();
		this.mainScrollPane.setContent(canvas);

		HBox statusBox = new HBox(15);
		statusBox.setAlignment(Pos.CENTER_LEFT);
		statusBox.setPadding(new Insets(3, 0, 3, 20));

		epochLbl = new Label("");
		statusBox.getChildren().addAll(new Label("Epoch: "),epochLbl);

		aliveLbl = new Label("");
		statusBox.getChildren().addAll(new Label("Alive: "),aliveLbl);

		elapsedLbl = new Label("");
		statusBox.getChildren().addAll(new Label("Elapsed Time: "),elapsedLbl);

		avgLbl = new Label("");
		statusBox.getChildren().addAll(new Label("Average Time: "),avgLbl);



		((VBox) scene.getRoot()).getChildren().addAll(scrollButtonPane, mainScrollPane, statusBox);

		return scene;

	}

	public void setController(PeaceMaker controller) {
		this.controller = controller;
	}

	public void updateBulk(int remaning) {
		Platform.runLater(() -> {
			this.bulkTxt.setText(remaning+"");
			if (remaning <= 1) {
				this.bulkTxt.setText(Settings.BULK+"");
				this.bulkTxt.setDisable(false);
				this.bulkStopBtn.setDisable(true);
				this.bulkStartBtn.setDisable(false);
				this.stopBtn.fireEvent(new ActionEvent());
			}
		});
	}

	@Override
	public void handle(ActionEvent ev) {
		Object src = ev.getSource();
		if (src.equals(startBtn)) {
			new Thread(() -> {
				this.stopBtn.setDisable(false);
				this.startBtn.setDisable(true);
				this.bulkStartBtn.setDisable(true);
				this.controller.launch();
			}).start();
		} else if (src.equals(stopBtn)) {
			this.stopBtn.setDisable(true);
			this.startBtn.setDisable(false);
			this.bulkStartBtn.setDisable(false);
			this.controller.pause();
		} else if (src.equals(bulkStartBtn)) {
			this.bulkTxt.setDisable(true);
			this.bulkStartBtn.setDisable(true);
			this.bulkStopBtn.setDisable(false);
			this.controller.setBulk(Integer.parseInt(this.bulkTxt.getText()));
			this.startBtn.fireEvent(new ActionEvent());
		} else if (src.equals(bulkStopBtn)) {
			this.controller.setBulk(1);
		}
	}

}
