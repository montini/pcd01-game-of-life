package main;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

import model.Settings;

public class Worker extends Thread {
	
	private byte[][] updatedMatrix;
	private byte[][] matrix;
	private int size;
	private int aliveCount;

	private int firstCol;
	private int lastCol;
	
	private Semaphore canPrint;
	private Semaphore canEvaluate;
	private CyclicBarrier workerBarrier;

	public Worker(int firstRow, int lastRow, Semaphore canPrint, Semaphore canEvaluate, byte[][] matrix, CyclicBarrier workerBarrier) {
		this.firstCol = firstRow;
		this.lastCol = lastRow;
		this.updatedMatrix = new byte[lastRow-firstRow][Settings.SIZE];
		this.size = Settings.SIZE;
		this.matrix = matrix;
		
		this.canPrint = canPrint;
		this.canEvaluate = canEvaluate;
		this.workerBarrier = workerBarrier;
	}

	@Override
	public void run() {
		while(true) {
			
			try {
				this.canEvaluate.acquire();
			} catch (InterruptedException e1) {}	
			
			for (int i = this.firstCol; i < this.lastCol; i++) {
				for (int j = 0; j<this.size; j++) {
					byte next = getNextGen(i,j);
					this.aliveCount += (int) next;
					this.updatedMatrix[i-firstCol][j] = next;
				}
			}
			
			try {
				this.workerBarrier.await();
			} catch (InterruptedException | BrokenBarrierException e) {e.printStackTrace();}

			for(int i = firstCol; i < lastCol; i++) {
				this.matrix[i] = updatedMatrix[i-firstCol].clone();
			}			
						
			canPrint.release();
		}
	}
	
	public int getAliveCount() {
		int alive = this.aliveCount;
		this.aliveCount = 0;
		return alive;
	}
	
	private byte getNextGen(int i, int j) {
		int neighbors = countNeighbors(i, j);
		
		if(neighbors < 2 || neighbors > 3){
			return 0;
		}else if(neighbors == 3){
			return 1;
		}
		return this.matrix[i][j];
	}
	
	private int countNeighbors(int i, int j) {
		int neighbors = 0;
		for (int a = i-1; a<i+2; a++) {
			for (int b = j-1; b < j+2; b++) {
				try {
					neighbors  += this.matrix[a][b];
				} catch (ArrayIndexOutOfBoundsException e) { }
			}
		}
		neighbors -= this.matrix[i][j];
		return neighbors;
	}
}
