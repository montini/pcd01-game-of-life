package main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import model.Settings;
import view.GameGUI;

public class PeaceMaker extends Thread {
	
	private List<Semaphore> canPrint;
	private List<Semaphore> canEvaluate;
	
	private CyclicBarrier workerBarrier;
	
	private byte[][] matrix;
	
	private List<Worker> workers;
	private GameGUI display;
	private int counter = 0;
	private List<Long> performance;
	
	private boolean running = false;
	private int bulkCounter = 0;
	
	private long millis;
	private long elapsedTime = 0;
	private double avg = 0;

	public PeaceMaker(GameGUI display) {
		this.display = display;
		this.matrix = Settings.getMatrix();
	}

	@Override
	public void run() {
		canPrint = new ArrayList<>();
		canEvaluate = new ArrayList<>();
		this.workers = new ArrayList<>();
		workerBarrier = new CyclicBarrier(Settings.NUM_THREADS);
			
		int cols = (int) Math.ceil( ((double)Settings.SIZE) / Settings.NUM_THREADS);
		
		for (int i = 0; i < Settings.NUM_THREADS; i++) {
			int first = cols*i;
			int last = (i < Settings.NUM_THREADS-1 ? cols*(i+1) : Settings.SIZE);
			
			Worker worker = createWorker(first, last);
			this.workers.add(worker);
			worker.start();
		}

		this.performance = new ArrayList<>();
		while(true) {	
			
			millis = System.currentTimeMillis();
			canPrint.forEach(sm -> {
				try {sm.acquire();} catch (InterruptedException e) {e.printStackTrace();}
			});
			
			workerBarrier.reset();
			
			if (this.bulkCounter == 0) {
				updateUserInterface();
			}
			
			this.display.updateAlive(this.workers.stream().mapToInt(w -> w.getAliveCount()).sum());
			
			canEvaluate.forEach(Semaphore::release);
			elapsedTime = System.currentTimeMillis() - millis;
		
			this.performance.add(elapsedTime);
				
			if (this.bulkCounter == 0) {
				if (Settings.WAIT_MS > 0) {
					try {
						Thread.sleep(Settings.WAIT_MS);
					} catch (InterruptedException e) {e.printStackTrace();}
				}
			} else {
				this.display.updateBulk(this.bulkCounter--);
				if (this.bulkCounter == 0) {
					updateUserInterface();
					this.display.updateAlive(this.workers.stream().mapToInt(w -> w.getAliveCount()).sum());
				}
			}
			
			this.counter++;
			computeAvg();
		}
	}
	
	public void launch() {
		if (!running) {
			this.canPrint.forEach(Semaphore::release);
			this.millis = System.currentTimeMillis();
			this.running = true;
		}
	}
	
	public void pause() {
		new Thread(() -> {
			if (running) {
				canPrint.forEach(sm -> {
					try {sm.acquire();} catch (InterruptedException e) {e.printStackTrace();}
				});
				this.running = false;
			}
		}).start();		
	}
	
	public void setBulk(int epochs) {
		this.bulkCounter = epochs;
	}

	public byte[][] getMatrix() {
		return this.matrix;
	}
	
	private void updateUserInterface() {
		this.display.refreshMatrix();
		this.display.updateCount(this.counter);
		this.display.updateElapsed(elapsedTime);
		this.display.updateAvg(avg);
	}

	private void computeAvg() {
		this.avg = (double) Math.round(this.performance.parallelStream()
				.skip(1)
				.mapToLong(l -> l)
				.average()
				.orElse(0)*100)/100;
	}
	
	private Worker createWorker(int firstCol, int lastCol) {
		canPrint.add(new Semaphore(0));
		canEvaluate.add(new Semaphore(0));
		int index = canPrint.size()-1;
		return new Worker(firstCol,lastCol, canPrint.get(index), canEvaluate.get(index), matrix, workerBarrier);
	}	

}
