package model;

import java.util.Arrays;
import java.util.Random;

public class Settings {
	
	public static int SIZE = 500;
	public static Pattern PATTERN = Pattern.RANDOM;
	public static int NUM_THREADS = 5;
	public static int WAIT_MS = 500;
	public static int SCALE = 1;
	public static int BULK = 100;
	
	private static byte[][] MATRIX;
	
	public static void createMatrix() {
				
		byte[][] matrix = new byte[SIZE][SIZE];
		for (int i = 0; i < SIZE; i++)
			Arrays.fill(matrix[i], (byte) 0);
		
		Random random = new Random(840488);
		switch (PATTERN) {
		case EMPTY:	
			
		break;
		case BLINKER:
			matrix[0][0] = 1;
			matrix[0][1] = 1;
			matrix[1][0] = 1;
			
			matrix[3][3] = 1;
			matrix[3][2] = 1;
			matrix[2][3] = 1;
			
			matrix[5][7] = 1;
			matrix[5][8] = 1;
			matrix[5][9] = 1;
			
			matrix[20][7] = 1;
			matrix[20][8] = 1;
			matrix[20][9] = 1;
			
		break;
		case RANDOM:
			for (int i = 0; i < SIZE; i++)
				for (int j = 0; j < SIZE; j++)
					matrix[i][j] = (byte) (random.nextDouble() < 0.165 ?  1 : 0);
		break;
		
		case LINES:
			random.setSeed(System.nanoTime());
			for (int i = 0; i < SIZE; i+=55) {
				for (int j = 0; j < SIZE; j++) {
					matrix[i][j] = (byte) (random.nextDouble() < 0.80 ?  1 : 0);
				}
			}
			for (int i = 0; i < SIZE; i++) {
				for (int j = 0; j < SIZE; j+=55) {
					matrix[i][j] = (byte) (random.nextDouble() < 0.80 ?  1 : 0);
				}
			}
			break;
		}
	
		MATRIX=matrix;
	}
	
	public static byte[][] getMatrix() {
		byte [][] copy = new byte[SIZE][];
		for (int i=0; i < SIZE; i++)
			copy[i] = MATRIX[i].clone();
		return copy;
	}

	public static void updateMatrix(byte[][] matrix, int firstCol, int lastCol) {
		for(int i = firstCol; i < lastCol; i++) {
			Settings.MATRIX[i] = matrix[i-firstCol].clone();
		}
	}
	
}
